# SFH4UP Secure Firefox Hardening 4 UX and Privacy
# Improvements
Use this project to improve your Firefox browser security and privacy while keeping it cool 😎️

![project banner](https://codeberg.org/repo-avatars/a54c247903f5fc40a1077ef32f309d533450a65733ba0d781cf9be428472b40e)

How does **SFH4UP** improve your Firefox browser? 
- update tracking configuration and disable telemetry to stay more anonymous 😎️
- fix configuration for cryptography for better security against data-theft 
- disable unnecessary features for better performance ⚡️⚡️⚡️
- disable Mozilla experiments ☢️ for better stability
# Installing
It is advised to backup your Firefox profile (create a copy of the profile directory) before configuring SFH4UP.
## Manual install
1. find the directory with your Firefox profiles
- in Windows the path looks like `C:\Users\YOUR_WINDOWS_USERNAME\AppData\Local\Mozilla\Firefox\Profiles`
- in MacOS and Linux the path looks like `/home/YOUR_LINUX_USERNAME/.mozilla/firefox/`
2. find the directory for the profile that you are using for Firefox
- in the most probable situation you have just one profile configured for firefox, so the directory looks like `YOUR-PROFILE-ID.default`
- examples: `miyo1Her.default`, `iuCeit2w.default`, `od7yaVae.default`
3. copy `user.js` file from this git repository in the profile directory from your PC
4. restart Firefox 
## More improvements
- install some of privacy addons and adblockers https://codeberg.org/fr333w/awesome-firefox-addons
- use **DNS over HTTPS**: Firefox Settings, Privacy & Security, DNS over HTTPS, set Max Protection
# Check your browser privacy
Find out what details your browser sends when you visit a website:
- https://coveryourtracks.eff.org/
- https://whatismybrowser.com/
- https://browseraudit.com/
- https://browserleaks.com/
- https://www.deviceinfo.me/
- https://bromite.org/detect
# Contribute
- improve SFH4UP configuration files ✌️ for Firefox: create pull requests with your changes or create a new issue to suggest changes
- donate ♥️ BTC bc1q66dx9szvmm4q38jucm7qls3rqulhkd9zsrxlp3

